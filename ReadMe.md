# Handlebars.js Lightning demo

See Script.md  

http://handlebarsjs.com

Use triple-stash to avoid escaping expressions: {{{body}}}  
```
<script id="entry-template" type="text/x-handlebars-template">

	{{! This is a comments. }}

	{{#each key.db}}
		<span class="comma-separated">{{this}}</span>
	{{/each}}

	{{../info}}

	{{#with key}}
		{{#each db}}
			<span class="comma-separated">{{this}}</span>
		{{/each}}
	{{/with}}

	{{#if key.db}}
	{{else}}
	{{/if}}

	{{#unless license}}
		<h3 class="warning">WARNING: This entry does not have a license!</h3>
	{{/unless}}

	Handlebars.registerHelper('fullName', function(person) {
	  return person.firstName + " " + person.lastName;
	});
	{{fullName author}}
</script>
```

# Mustache.js lightning demo

http://mustache.github.io/

Manual: http://mustache.github.io/mustache.5.html  

# Rivets.js
2-way binding  
http://rivetsjs.com/
