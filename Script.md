## Open:
TextWrangler: Script.md  
BBEdit: cv.html  
Eclipse Juno: HelloMustache + template.mustache  
http://handlebarsjs.com  
http://mustache.github.io  
http://rivetsjs.com  


Handlebars.js
================================================

Start with cv.html  
Short intro on plumbing  
Short intro on http://handlebarsjs.com  
Open in FireFox (side by side with code)  


## Paste in template
<script id="entry-template" type="text/x-handlebars-template">
  <div class="body">
		<h1>Curriculum Vitae for ...</h1>
  </div>
</script>

## Paste in script
<script>
	// json to render
	var tsk = {'_id' : 'tsk',
	  'fn' : 'Tommy', 'ln' : 'Skodje',
	  'intro' : 'Tommy er <b>kjekk og grei</b>.',
	  'key' : {
			'db' : [ 'sql', 'mongoDb', 'jdbc', 'spring' ]
	  }
	};

	// get template and compile it
	var source   = $( "#entry-template" ).html();
	var template = Handlebars.compile( source );

	// evaluate the template against json
	var evaluated	= template( tsk );

	// write result into dom
	$("#replace").html( evaluated );
</script>

## Add handlebars expression
{{fn}} {{ln}}

## Add iterator expression
		<h3>Key Competencies</h3>
		<b>Database: </b>
		{{#each key.db}}
			<span class="comma-separated">{{this}}</span>
		{{/each}}

## Add triple-stash to intro
		<p>{{intro}}</p>

Comment: html-escaping

Mustache
================================================
BBEdit: mustache.html  
Eclipse: HelloMustache + template.mustache (Side by side)  

http://mustache.github.io/
