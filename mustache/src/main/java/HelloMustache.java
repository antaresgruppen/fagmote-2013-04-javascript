import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

public class HelloMustache {

  List<Item> items() {
    return Arrays.asList(
      new Item("Item 1", "$19.99", Arrays.asList(new Feature("New!"), new Feature("Awesome!"))),
      new Item("Item 2", "$29.99", Arrays.asList(new Feature("Old."), new Feature("Ugly.")))
    );
  }

  static class Item {
    Item(String name, String price, List<Feature> features) {
      this.name = name;
      this.price = price;
      this.features = features;
    }

    String name, price;
    List<Feature> features;
  }

  static class Feature {
    Feature(String description) {
      this.description = description;
    }

    String description;
  }

  /** */
  public static void external_template_with_HelloMustache_object() throws IOException {
    MustacheFactory mf = new DefaultMustacheFactory();

    Mustache mustache = mf.compile("template.mustache");

    Writer stdout = new OutputStreamWriter(System.out);
    mustache.execute( stdout, new HelloMustache() );
    stdout.flush();
  }

  /**. */
  public static void hard_coded_template_with_Map() throws IOException {
    MustacheFactory mf = new DefaultMustacheFactory();

    HashMap<String, Object> scopes = new HashMap<String, Object>();
    scopes.put("name", "Mustache");
    scopes.put("feature", new Feature("Perfect!"));

    Mustache mustache = mf.compile(new StringReader("{{name}}, {{feature.description}}!"), "example");

    Writer stdout = new OutputStreamWriter(System.out);
    mustache.execute(stdout, scopes);
    stdout.flush();
  }

  /** external template evaluated with HelloMustache object. */
  public static void main(String[] args) throws IOException {
  	System.out.println( "External template evaluated with HelloMustache object:\n" );
  	external_template_with_HelloMustache_object();

  	System.out.println( "\n\nHard-coded template evaluated with Map:\n" );
  	hard_coded_template_with_Map();
  }

}